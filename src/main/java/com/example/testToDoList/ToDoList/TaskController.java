package com.example.testToDoList.ToDoList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/task")
public class TaskController {
    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }
    @GetMapping()
    public List<Task> getTask() {
        return taskService.getTask();

    }
    @PostMapping
    public void registrationNewTask(@RequestBody Task task){
        taskService.addNewTask(task);
    }
    @DeleteMapping(path = "{taskId}")
    public void deletePlayer(@PathVariable("taskId") Long taskId) {
        taskService.deleteTaskId(taskId);
    }
}
