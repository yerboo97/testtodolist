package com.example.testToDoList.ToDoList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    private final TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getTask() {
        return taskRepository.findAll();

    }

    public void addNewTask(Task task) {
        Optional<Task> taskById = taskRepository.findTaskById(task.getId());
        if (taskById.isPresent()) {
            throw new IllegalStateException("id taken");
        }
        taskRepository.save(task);

    }

    public void deleteTaskId(Long taskId) {
        taskRepository.findById(taskId);
        boolean exists = taskRepository.existsById(taskId);
        if (!exists) {
            throw new IllegalStateException("task with id " + taskId + " does not exists");
        }
        taskRepository.deleteById(taskId);
    }
}

