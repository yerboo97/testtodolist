package com.example.testToDoList.ToDoList;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class TaskConfig {
    @Bean
    CommandLineRunner commandLineRunner(TaskRepository repository) {
        return args -> {
            Task football = new Task(
                    "Football Germany vs France",
                    "15.06.2021",
                    "important");

            Task volleyball = new Task(
                    "volleyball Germany vs France",
                    "15.06.2021",
                    "notImportant");

            repository.saveAll(
                    List.of(football,volleyball)
            );

        };


    }

}

