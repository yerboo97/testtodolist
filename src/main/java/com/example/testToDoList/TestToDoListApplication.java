package com.example.testToDoList;

import com.example.testToDoList.ToDoList.Task;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@SpringBootApplication
public class TestToDoListApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestToDoListApplication.class, args);
	}


}
